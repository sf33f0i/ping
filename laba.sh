#!/bin/bash

rm -f ./md1/*

function convert {
    curl \
    https://www.docverter.com 
    --form input_files[]=@$1 \
    --form from=markdown \
    --form to=pdf \
    http://c.docverter.com/convert >$2
}

function parse_line {
    sub='-c'
    sub1='-c 4'
    echo Input string: "$1"
    local IFS=" "
    read -ra arr <<<"$1"
    host=${arr[0]}
    echo $host
    param=''
    local len=${#arr[@]}
    for (( i=1; i < $len; i+=1 )); do 
    param="$param ${arr[$i]}"
    done

    echo param: $param
	if [[ "$param" == *"$sub"* ]]; then
    echo "-c присутствует"
    else
    param1="$param$sub1"
    param=$param1
    fi
	echo param: $param
}

function test_host {
home="./md1"
if [ -e $home ]; then
echo "каталог существут"
else
mkdir $home
fi
out_file=./md1/otchet.md 
echo "# test connect: $host " >>$out_file
echo '' >>$out_file
echo "**run ping** " >>$out_file
echo '' >>$out_file
echo \`\`\`bash >>$out_file
ping $host $param >>$out_file
echo \`\`\` >>$out_file
echo '' >>$out_file
if  [[ ping -c 1 $host &> /dev/null ]]; then
echo "**run traceroute** " >>$out_file
echo '' >>$out_file
echo \`\`\`bash >>$out_file
traceroute $host >>$out_file
echo \`\`\` >>$out_file
fi
convert $out_file "./otchet.pdf"
}

while read line; do 
parse_line "$line"
test_host
done <./ip_linux.txt